import { sayHello } from "../src/say-hello";

describe('sayHello', function() {
    it("should return world", function() {
        expect(sayHello("World")).toBe("Hello World");
    });
});